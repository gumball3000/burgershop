package com.company;

public class Addition {

    private String addition1name;
    private double addition1price;
    private String addition2name;
    private double addition2price;
    private String addition3name;
    private double addition3price;
    private String addition4name;
    private double addition4price;
    private String addition5name;
    private double addition5price;
    private String addition6name;
    private double addition6price;
    private String addition7name;
    private double addition7price;


    public double getPriceByIndex(int additionIndexNumber){
        if (additionIndexNumber>=0&&additionIndexNumber<=7){
            switch(additionIndexNumber){
                case 1:
                    return this.addition1price;

                case 2:
                    return this.addition2price;

                case 3:
                    return this.addition3price;

                case 4:
                    return this.addition4price;

                case 5:
                    return this.addition5price;

                case 6:
                    return this.addition6price;

                case 7:
                    return this.addition7price;

                default:
                    return 0.0;

            }
        }
        return 0.0;
    }

    public String getNameByIndex(int additionIndexNumber){
        if (additionIndexNumber>=0&&additionIndexNumber<=7){
            switch(additionIndexNumber){
                case 1:
                    return this.addition1name;

                case 2:
                    return this.addition2name;

                case 3:
                    return this.addition3name;

                case 4:
                    return this.addition4name;

                case 5:
                    return this.addition5name;

                case 6:
                    return this.addition6name;

                case 7:
                    return this.addition7name;

                default:
                    return null;

            }
        }
        return null;
    }

    public String getAddition1name() {
        return addition1name;
    }

    public double getAddition1price() {
        return addition1price;
    }

    public String getAddition2name() {
        return addition2name;
    }

    public double getAddition2price() {
        return addition2price;
    }

    public String getAddition3name() {
        return addition3name;
    }

    public double getAddition3price() {
        return addition3price;
    }

    public String getAddition4name() {
        return addition4name;
    }

    public double getAddition4price() {
        return addition4price;
    }

    public String getAddition5name() {
        return addition5name;
    }

    public double getAddition5price() {
        return addition5price;
    }

    public String getAddition6name() {
        return addition6name;
    }

    public double getAddition6price() {
        return addition6price;
    }

    public String getAddition7name() {
        return addition7name;
    }

    public double getAddition7price() {
        return addition7price;
    }

    public void setAddition(int additionIndexNumber, String additionName, double additionPrice){
        if (additionIndexNumber>=0&&additionIndexNumber<=7){
            switch(additionIndexNumber){
            case 1:
                this.addition1name = additionName;
                this.addition1price = additionPrice;
                break;

            case 2:
                this.addition2name = additionName;
                this.addition2price = additionPrice;
                break;

            case 3:
                this.addition3name = additionName;
                this.addition3price = additionPrice;
                break;

            case 4:
                this.addition4name = additionName;
                this.addition4price = additionPrice;
                break;

            case 5:
                this.addition5name = additionName;
                this.addition5price = additionPrice;
                break;

            case 6:
                this.addition6name = additionName;
                this.addition6price = additionPrice;
                break;

            case 7:
                this.addition7name = additionName;
                this.addition7price = additionPrice;
                break;

                default:
                    break;

            }
        }
    }

}
