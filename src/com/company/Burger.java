package com.company;

public class Burger {
    private Addition addition;
    private Burgertype burgertype;
    private double totalPrice = 0;
    private double buyingPrice = 0;

//    public double getBurgerTotalPrice(int indexNumber){
//        if(indexNumber==1){
//
//        }
//    }

    public void incrementBuyingPrice(double x){
        buyingPrice+=x;
    }

    public void displayBurgerAndAdditions(){
        if(burgertype.getBurgerIndexNumber()==1) {
            System.out.println( "you have selected the " + getBurgertype().getName() + " made with "+getBurgertype().getBread()+" and "+getBurgertype().getMeat()+", this burger has base price of " + getBurgertype().getBasePrice() + " RON and a total price of " + totalPrice + " RON if you buy all the following toppings:" );
            for (int i = 1; i <= 4; i++) {
                System.out.println( i + "." + getAddition().getNameByIndex( i ) + " Price: " + getAddition().getPriceByIndex( i ) );
            }
        }
        if(burgertype.getBurgerIndexNumber()==2){
            System.out.println("you have selected the "+getBurgertype().getName()+ " made with "+getBurgertype().getBread()+" and "+getBurgertype().getMeat()+", this burger has base price of "+getBurgertype().getBasePrice()+" RON and a total price of "+totalPrice+" RON if you buy all the following toppings:");
            for(int i=1;i<=6; i++){
                System.out.println(i+"."+getAddition().getNameByIndex( i )+" Price: "+getAddition().getPriceByIndex( i ) );
            }
        }
        if(burgertype.getBurgerIndexNumber()==3){
            System.out.println("you have selected the "+getBurgertype().getName()+ " made with "+getBurgertype().getBread()+" and "+getBurgertype().getMeat()+", this burger has a total price of "+totalPrice+" RON the "+getAddition().getNameByIndex( 7 )+" are included in the price");
        }
    }


    public Burger(Addition addition, Burgertype burgertype) {
        this.addition = addition;
        this.burgertype = burgertype;
                if(burgertype.getBurgerIndexNumber()==1){
                totalPrice+=burgertype.getBasePrice();
                for(int i=1;i<=4; i++){
                    totalPrice+=addition.getPriceByIndex( i );
                    }

                }
        if(burgertype.getBurgerIndexNumber()==2){
            totalPrice+=burgertype.getBasePrice();
            for(int i=1;i<=6; i++){
                totalPrice+=addition.getPriceByIndex( i );
            }

        }
        if(burgertype.getBurgerIndexNumber()==3){
            totalPrice+=burgertype.getBasePrice();
            totalPrice+=getAddition().getPriceByIndex( 7 );

        }
        buyingPrice = getBurgertype().getBasePrice();
    }

    public Addition getAddition() {
        return addition;
    }

    public Burgertype getBurgertype() {
        return burgertype;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public double getBuyingPrice() {
        return buyingPrice;
    }
}
