package com.company;

public class Burgertype {
    private String name;
    private String bread;
    private String meat;
    private int additionsAllowed;
    private double basePrice;
    private int burgerIndexNumber;

    public Burgertype(String name, String bread, String meat, int additionsAllowed, double basePrice, int burgerIndexNumber) {
        this.name = name;
        this.bread = bread;
        this.meat = meat;
        this.additionsAllowed = additionsAllowed;
        this.basePrice = basePrice;
        this.burgerIndexNumber = burgerIndexNumber;
    }

    public String getName() {
        return name;
    }

    public String getBread() {
        return bread;
    }

    public String getMeat() {
        return meat;
    }

    public int getAdditionsAllowed() {
        return additionsAllowed;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public int getBurgerIndexNumber() {
        return burgerIndexNumber;
    }
}
